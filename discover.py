import json
import os
import flux_led

DISCOVER_FILE = './lights.json'


def main():
    scanner = flux_led.BulbScanner()

    found_lights = scanner.scan(0.2)

    if file_exists():
        stored_lights = get_lights()

        for stored_light in stored_lights:
            # print(stored_light['ipaddr'])
            for found_light in found_lights:
                if stored_light['id'] in found_light['id']:
                    if 'name' in stored_light:
                        found_light['name'] = stored_light['name']
                    else:
                        found_light['name'] = 'Name this light'

    save_file(json.dumps(found_lights, indent=4, sort_keys=True))


def save_file(contents):
    file = open(DISCOVER_FILE, 'w')
    file.write(contents)
    file.close()


def file_exists():
    return os.path.exists(DISCOVER_FILE)


def get_lights():
    file = open(DISCOVER_FILE, 'r')
    contents = json.loads(file.read())
    file.close()
    return contents


if __name__ == "__main__":
    main()
