import json

import discover
from light import Light


class Lights:
    lights = []

    def __init__(self):

        if discover.file_exists():
            lights = discover.get_lights()

            for light in lights:
                self.lights.append(Light(light))

    def json(self):
        lights = []
        for light in self.lights:
            lights.append(light.json())

        return json.dumps(lights)

    def off(self):
        for light in self.lights:
            light.off()

    def on(self):
        for light in self.lights:
            light.on()
