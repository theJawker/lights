from bottle import route, run, template
from lights import Lights


@route('/lights')
def index():
    lights = Lights()
    return template(lights.json())


@route('/lights/on')
def index():
    lights = Lights()
    print(lights.on())


@route('/lights/off')
def index():
    lights = Lights()
    print(lights.off())


run(server='cherrypy', host='0.0.0.0', port=8080, reloader=True, interval=2, debug=True)
