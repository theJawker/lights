import flux_led


class Light:
    def __init__(self, array):
        self.id = array['id']
        self.ipaddr = array['ipaddr']
        self.model = array['model']
        self.name = array['name']
        try:
            self.bulb = flux_led.WifiLedBulb(self.ipaddr, timeout=1)
        except TypeError:
            pass

    def off(self):
        self.bulb.turnOff()

    def on(self):
        self.bulb.turnOn()

    def json(self):
        return {
            'id': self.id,
            'ipaddr': self.ipaddr,
            'model': self.model,
            'name': self.name,
        }
